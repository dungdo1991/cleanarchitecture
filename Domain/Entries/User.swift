//
//  User.swift
//  Domain
//
//  Created by Dung Do on 28/12/2021.
//

import Foundation
import RealmSwift

public enum Gender: Int, Codable, PersistableEnum {
    case unknown
    case man
    case woman
}

public class Address: EmbeddedObject, Codable {
    @Persisted public var city: String
    @Persisted public var country: String
    
    public convenience init(city: String, country: String) {
        self.init()
        self.city = city
        self.country = country
    }
    
}

public class User: Object, Codable {
    
    @Persisted public var id = UUID()
    @Persisted public var name: String
    @Persisted public var age: Int
    @Persisted(primaryKey: true) public var email: String
    @Persisted public var password: String
    @Persisted public var gender: Gender
    @Persisted public var address: Address?
    
    enum CodingKeys: CodingKey {
        case id
        case name
        case age
        case email
        case password
        case gender
        case address
    }
    
    public convenience init(id: UUID, name: String, age: Int, email: String, password: String, gender: Gender, address: Address?) {
        self.init()
        self.id = id
        self.name = name
        self.age = age
        self.email = email
        self.password = password
        self.gender = gender
        self.address = address
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(name, forKey: .name)
        try container.encode(age, forKey: .age)
        try container.encode(email, forKey: .email)
        try container.encode(password, forKey: .password)
        try container.encode(gender, forKey: .gender)
        try container.encode(address, forKey: .address)
    }
    
    required public convenience init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let id = try container.decode(UUID.self, forKey: .id)
        let name = try container.decode(String.self, forKey: .name)
        let age = try container.decode(Int.self, forKey: .age)
        let email = try container.decode(String.self, forKey: .email)
        let password = try container.decode(String.self, forKey: .password)
        let gender = try container.decode(Int.self, forKey: .gender)
        let address = try container.decode(Address?.self, forKey: .address)
        self.init(id: id, name: name, age: age, email: email, password: password, gender: Gender(rawValue: gender) ?? .unknown, address: address)
    }
    
    public func toJSON() -> [String: Any?] {
        return [
            "id": id,
            "name": name,
            "age": age,
            "email": email,
            "password": password,
            "gender": gender,
            "address": address,
        ]
    }
    
    public func toData() -> Data {
        return try! JSONEncoder().encode(self)
    }
    
}
