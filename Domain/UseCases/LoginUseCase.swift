//
//  LoginUseCase.swift
//  Domain
//
//  Created by Dung Do on 28/12/2021.
//

import Foundation
import RxSwift

public protocol LoginUseCase {
    func signUp(user: User) -> Observable<Void>
    func login(email: String, password: String) -> Observable<User>
    func logout(email: String, password: String) -> Observable<Void>
    func users() -> Observable<[User]>
    func deleteUser(email: String) -> Observable<Void>
}
