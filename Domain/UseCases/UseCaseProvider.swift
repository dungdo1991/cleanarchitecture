//
//  UseCaseProvider.swift
//  Domain
//
//  Created by Dung Do on 28/12/2021.
//

import Foundation

public protocol UseCaseProvider {
    func createLoginUseCase() -> LoginUseCase
}
