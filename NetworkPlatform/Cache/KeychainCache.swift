//
//  KeychainCache.swift
//  NetworkPlatform
//
//  Created by Dung Do on 06/01/2022.
//

import Foundation
import RxSwift
import KeychainSwift
import Domain

class KeychainCache<T: Codable> {
    
    private lazy var keychain = KeychainSwift()
    
    func set(object: T, key: String) -> Completable {
        return Completable.create { observer in
            if let data = try? JSONEncoder().encode(object) {
                self.keychain.set(data, forKey: key)
                observer(.completed)
            } else {
                observer(.error(NSError(domain: "Test",
                                        code: -1,
                                        userInfo: [NSLocalizedDescriptionKey : "Not found!!!"])))
            }
            
            return Disposables.create()
        }
    }
    
    func getObject(key: String) -> Maybe<T> {
        return Maybe.create { observer in
            if let data = self.keychain.getData(key),
               let object = try? JSONDecoder().decode(T.self, from: data) {
                observer(.success(object))
            } else {
                observer(.error(NSError(domain: "Test",
                                        code: -1,
                                        userInfo: [NSLocalizedDescriptionKey : "Not found!!!"])))
            }
            
            return Disposables.create()
        }
    }
    
    func getUsers() -> Maybe<[T]> {
        return Maybe.create { observer in
            var arr = Array<User>(repeating: User(id: UUID(), name: "Dung", age: 1, email: "dung@gmail.com", password: "123456", gender: .man, address: nil), count: 10)
            arr.append(contentsOf: Array<User>(repeating: User(id: UUID(), name: "Thien", age: 1, email: "thien@gmail.com", password: "123456", gender: .woman, address: nil), count: 10))
            arr.append(contentsOf: Array<User>(repeating: User(id: UUID(), name: "Huy", age: 1, email: "huy@gmail.com", password: "123456", gender: .unknown, address: nil), count: 10))
            
            if let data = try? JSONEncoder().encode(arr),
                let object = try? JSONDecoder().decode([T].self, from: data) {
                observer(.success(object))
            } else {
                observer(.error(NSError(domain: "Test",
                                        code: -1,
                                        userInfo: [NSLocalizedDescriptionKey : "Not found!!!"])))
            }
            
            return Disposables.create()
        }.delay(.seconds(1), scheduler: MainScheduler.instance)
    }
    
    func delete(key: String) -> Completable {
        Completable.create { observer in
            if self.keychain.delete(key) {
                observer(.completed)
            } else {
                observer(.error(NSError(domain: "Test",
                                        code: -1,
                                        userInfo: [NSLocalizedDescriptionKey : "Not found!!!"])))
            }
            return Disposables.create()
        }
    }
    
}
