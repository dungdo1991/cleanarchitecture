//
//  SQLiteCache.swift
//  NetworkPlatform
//
//  Created by Dung Do on 23/09/2022.
//

import Foundation
import RxSwift
import SQLite
import Domain

class SQLiteCache {
    var database: Connection?
    let table_user = Table("Users")
    let id = Expression<UUID>("Id")
    let name = Expression<String>("Name")
    let age = Expression<Int>("Age")
    let email = Expression<String>("Email")
    let password = Expression<String>("Password")
    let gender = Expression<Int>("Gender")
    
    let table_address = Table("Address")
    let userId = Expression<UUID>("User_Id")
    let city = Expression<String>("City")
    let country = Expression<String>("Country")
    
    init() {
        createDatabase()
        createTable()
    }
    
    private func createDatabase() {
        let filePath = try! FileManager.default.url(for: .documentDirectory,
                                                    in: .userDomainMask,
                                                    appropriateFor: nil,
                                                    create: false).appendingPathComponent("MyDB").appendingPathExtension("sqlite3")
        if let database = try? Connection(filePath.path) {
            self.database = database
        } else {
            print("Can't create database")
        }
    }
    
    private func createTable() {
        do {
            try database?.run(table_user.create(ifNotExists: true) { t in
                t.column(id, primaryKey: true)
                t.column(name)
                t.column(age)
                t.column(email, unique: true)
                t.column(password)
                t.column(gender)
            })
            
            try database?.run(table_address.create(ifNotExists: true) { t in
                t.column(userId, primaryKey: true)
                t.column(city)
                t.column(country)
            })
        } catch {
            print(error)
        }
    }
    
    func set(object: User, key: String) -> Completable {
        return Completable.create { observer in
            let insertUser = self.table_user.insert(self.id <- object.id,
                                                    self.name <- object.name,
                                                    self.age <- object.age,
                                                    self.email <- object.email,
                                                    self.password <- object.password,
                                                    self.gender <- object.gender.rawValue)
            let insertAddress = self.table_address.insert(self.userId <- object.id,
                                                          self.city <- object.address?.city ?? "",
                                                          self.country <- object.address?.country ?? "")
            do {
                try self.database?.run(insertUser)
                try self.database?.run(insertAddress)
                observer(.completed)
            } catch let Result.error(message: message, code: code, statement: _) where code == SQLITE_CONSTRAINT {
                observer(.error(NSError(domain: "Test",
                                        code: -1,
                                        userInfo: [NSLocalizedDescriptionKey : message])))
            } catch {
                observer(.error(error))
            }
            
            return Disposables.create()
        }
    }
    
    func getObject(key: String) -> Maybe<User> {
        return Maybe.create { observer in
            let query = self.table_user.select([self.id, self.name, self.age, self.email, self.password, self.gender, self.city, self.country])
                .join(self.table_address, on: self.id == self.userId)
                .filter(self.email == key)
                .limit(1)
                
            do {
                for user in try self.database!.prepare(query) {
                    let user = User(id: user[self.id],
                                    name: user[self.name],
                                    age: user[self.age],
                                    email: user[self.email],
                                    password: user[self.password],
                                    gender: Gender(rawValue: user[self.gender]) ?? .unknown,
                                    address: Address(city: user[self.city], country: user[self.country]))
                    observer(.success(user))
                }
            } catch let Result.error(message: message, code: code, statement: _) where code == SQLITE_CONSTRAINT {
                observer(.error(NSError(domain: "Test",
                                        code: -1,
                                        userInfo: [NSLocalizedDescriptionKey : message])))
            } catch {
                observer(.error(error))
            }
            
            return Disposables.create()
        }
    }
    
    func getUsers() -> Maybe<[User]> {
        return Maybe.create { observer in
            let query = self.table_user.select([self.id, self.name, self.age, self.email, self.password, self.gender, self.city, self.country])
                .join(self.table_address, on: self.id == self.userId)
                
            do {
                var arr = Array<User>()
                for user in try self.database!.prepare(query) {
                    let user = User(id: user[self.id],
                                    name: user[self.name],
                                    age: user[self.age],
                                    email: user[self.email],
                                    password: user[self.password],
                                    gender: Gender(rawValue: user[self.gender]) ?? .unknown,
                                    address: Address(city: user[self.city], country: user[self.country]))
                    arr.append(user)
                }
                observer(.success(arr))
            } catch let Result.error(message: message, code: code, statement: _) where code == SQLITE_CONSTRAINT {
                observer(.error(NSError(domain: "Test",
                                        code: -1,
                                        userInfo: [NSLocalizedDescriptionKey : message])))
            } catch {
                observer(.error(error))
            }
            
            return Disposables.create()
        }.delay(.seconds(1), scheduler: MainScheduler.instance)
    }
    
    func delete(key: String) -> Completable {
        Completable.create { observer in
            let user = self.table_user.filter(self.email == key)
            let delete = user.delete()
            do {
                try self.database?.run(delete)
                observer(.completed)
            } catch let Result.error(message: message, code: code, statement: _) where code == SQLITE_CONSTRAINT {
                observer(.error(NSError(domain: "Test",
                                        code: -1,
                                        userInfo: [NSLocalizedDescriptionKey : message])))
            } catch {
                observer(.error(error))
            }
            
            return Disposables.create()
        }
    }
    
}
