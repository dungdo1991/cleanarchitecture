//
//  RealmCache.swift
//  NetworkPlatform
//
//  Created by Dung Do on 19/09/2022.
//

import Foundation
import RxSwift
import RealmSwift
import Domain

class RealmCache<T: Object & Codable> {
    
    private lazy var realm = try? Realm()
    
    init() {
        let config = Realm.Configuration(
            schemaVersion: 4,
            migrationBlock: { migration, oldSchemaVersion in
                if (oldSchemaVersion < 3) {
                    migration.enumerateObjects(ofType: Domain.User.className()) { oldObject, newObject in
                        newObject!["gender"] = Gender.unknown
                    }
                }
            })
        Realm.Configuration.defaultConfiguration = config
    }
    
    func set(object: T, key: String) -> Completable {
        return Completable.create { observer in
            try? self.realm?.write {
                self.realm?.add(object)
            }
            observer(.completed)
            
            return Disposables.create()
        }
    }
    
    func getObject(key: String) -> Maybe<T> {
        return Maybe.create { observer in
            if let object = self.realm?.object(ofType: T.self, forPrimaryKey: key) {
                observer(.success(object))
            } else {
                observer(.error(NSError(domain: "Test",
                                        code: -1,
                                        userInfo: [NSLocalizedDescriptionKey : "Not found!!!"])))
            }
            
            return Disposables.create()
        }
    }
    
    func getUsers() -> Maybe<[T]> {
        return Maybe.create { observer in
            if let objects = self.realm?.objects(T.self) {
                observer(.success(objects.map { $0 }))
            } else {
                observer(.error(NSError(domain: "Test",
                                        code: -1,
                                        userInfo: [NSLocalizedDescriptionKey : "Not found!!!"])))
            }
            
            return Disposables.create()
        }.delay(.seconds(1), scheduler: MainScheduler.instance)
    }
    
    func delete(key: String) -> Completable {
        Completable.create { observer in
            if let object = self.realm?.object(ofType: T.self, forPrimaryKey: key) {
                try? self.realm?.write {
                    self.realm?.delete(object)
                }
                observer(.completed)
            } else {
                observer(.error(NSError(domain: "Test",
                                        code: -1,
                                        userInfo: [NSLocalizedDescriptionKey : "Not found!!!"])))
            }
            
            return Disposables.create()
        }
    }
    
}
