//
//  Network.swift
//  NetworkPlatform
//
//  Created by Dung Do on 29/12/2021.
//

import Foundation
import RxSwift
import Alamofire
import Domain

class Network<T: Decodable> {
    
    func request(method: HTTPMethod, url: String, params: [String:Any?]) -> Observable<(T)> {
        let data = User(id: UUID(), name: "Test", age: 1, email: "test@gmail.com", password: "123456", gender: .unknown, address: nil).toData()
        let result = try! JSONDecoder().decode(T.self, from: data)
        
        return Observable.create { observer in
            if url == "user/register" {
                observer.onNext(result)
                observer.onCompleted()
            } else if url == "user/login" {
                observer.onNext(result)
                observer.onCompleted()
            } else if url == "user/logout" {
                observer.onNext(result)
                observer.onCompleted()
            } else {
                observer.onError(NSError(domain: "Test",
                                         code: -1,
                                         userInfo: [NSLocalizedDescriptionKey : "Something went wrong!!!"]))
            }
            return Disposables.create()
        }.delay(.seconds(1), scheduler: MainScheduler.instance)
    }
    
}
