//
//  UseCaseProvider.swift
//  NetworkPlatform
//
//  Created by Dung Do on 29/12/2021.
//

import Foundation
import Domain

public class UseCaseProvider: Domain.UseCaseProvider {
    
    public init() {}
    
    public func createLoginUseCase() -> Domain.LoginUseCase {
        let network = Network<User>()
        let cache = SQLiteCache()
        let usecase = LoginUseCase(network: network, cache: cache)
        return usecase
    }
    
}
