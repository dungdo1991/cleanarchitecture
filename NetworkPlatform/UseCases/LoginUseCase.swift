//
//  LoginUseCase.swift
//  NetworkPlatform
//
//  Created by Dung Do on 29/12/2021.
//

import Foundation
import RxSwift
import Alamofire
import Domain

class LoginUseCase: Domain.LoginUseCase {
    
    private let network: Network<User>
    private let cache: SQLiteCache
    
    init(network: Network<User>, cache: SQLiteCache) {
        self.network = network
        self.cache = cache
    }
    
    func signUp(user: User) -> Observable<Void> {
        let params = user.toJSON()
        return network
            .request(method: .post, url: "user/register", params: params)
            .flatMap { _ in
                self.cache.set(object: user, key: user.email)
                    .andThen(Observable.just(()))
            }
    }
    
    func login(email: String, password: String) -> Observable<User> {
        let params = [
            "email":email,
            "password":password,
        ]
        return network
            .request(method: .post, url: "user/login", params: params)
            .flatMap { _ in
                self.cache.getObject(key: email)
                    .flatMap { user in
                        if user.email == email && user.password == password {
                            return Maybe.just(user)
                        } else {
                            return Maybe.error(NSError(domain: "Test",
                                                           code: -1,
                                                           userInfo: [NSLocalizedDescriptionKey : "email or password not match!!!"]))
                        }
                    }
            }
    }
    
    func logout(email: String, password: String) -> Observable<Void> {
        let params = [
            "email":email,
            "password":password,
        ]
        return network
            .request(method: .post, url: "user/logout", params: params)
            .map { _ in }
    }
    
    func users() -> Observable<[User]> {
        return cache.getUsers().asObservable()
    }
    
    func deleteUser(email: String) -> Observable<Void> {
        return cache.delete(key: email).andThen(Observable.just(()))
    }
    
}
