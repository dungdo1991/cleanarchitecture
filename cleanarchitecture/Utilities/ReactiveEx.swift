//
//  ReactiveEx.swift
//  cleanarchitecture
//
//  Created by Dung Do on 10/01/2022.
//

import RxSwift
import RxCocoa

extension Reactive where Base: UIScrollView {
    
    var reachedBottom: Driver<Void> {
        return contentOffset
            .flatMapLatest { [weak base] contentOffset -> Observable<CGFloat> in
                guard let scrollView = base else {
                    return Observable.empty()
                }
                
                let visibleHeight = scrollView.frame.height - scrollView.contentInset.top - scrollView.contentInset.bottom
                let y = contentOffset.y + scrollView.contentInset.top
                let threshold = max(0.0, scrollView.contentSize.height - visibleHeight)
                
                return y > threshold ? Observable.just(scrollView.contentSize.height) : Observable.empty()
            }
            .debounce(.milliseconds(100), scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .mapToVoid()
            .asDriverOnErrorJustComplete()
    }
    
}
