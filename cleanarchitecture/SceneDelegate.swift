//
//  SceneDelegate.swift
//  cleanarchitecture
//
//  Created by Dung Do on 28/12/2021.
//

import UIKit
import Domain
import NetworkPlatform

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let scene = (scene as? UIWindowScene) else { return }
        
        let loginVC = LoginViewController()
        let nav = UINavigationController(rootViewController: loginVC)
        let provider = NetworkPlatform.UseCaseProvider()
        let navigation = DefaultLoginNavigator(useCaseProvider: provider, navigation: nav)
        loginVC.loginVM = LoginViewModel(useCase: provider.createLoginUseCase(), navigation: navigation)
        window = UIWindow(windowScene: scene)
        window?.rootViewController = nav
        window?.makeKeyAndVisible()
    }

}

