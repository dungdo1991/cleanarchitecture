//
//  BaseViewController.swift
//  cleanarchitecture
//
//  Created by Dung Do on 06/01/2022.
//

import UIKit
import RxSwift
import PKHUD

extension Notification.Name {
    static let changeLanguage = Notification.Name(rawValue: "changeLanguage")
}

class BaseViewController: UIViewController {
    
    let disposeBag = DisposeBag()
    
    var errorBinding: Binder<Error> {
        return Binder(self, binding: { (vc, error) in
            let alert = UIAlertController(title: "Error",
                                          message: error.localizedDescription,
                                          preferredStyle: .alert)
            let action = UIAlertAction(title: "OK",
                                       style: UIAlertAction.Style.cancel,
                                       handler: nil)
            alert.addAction(action)
            vc.present(alert, animated: true, completion: nil)
        })
    }
    
    var loadingBinding: Binder<Bool> {
        return Binder(self) { [weak self] (vc, isShow) in
            if isShow {
                HUD.show(.progress, onView: self?.view)
            } else {
                HUD.hide()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.rx.notification(.changeLanguage)
            .subscribe(onNext: { [weak self] _ in
                self?.configureLanguage()
            })
            .disposed(by: disposeBag)
    }

    func configureLanguage() {

    }
    
}
