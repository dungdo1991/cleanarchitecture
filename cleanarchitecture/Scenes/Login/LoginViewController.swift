//
//  LoginViewController.swift
//  cleanarchitecture
//
//  Created by Dung Do on 28/12/2021.
//

import UIKit
import RxSwift
import RxCocoa
import PKHUD

class LoginViewController: BaseViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var signUpButton: UIButton!
    
    var loginVM: LoginViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupRx()
    }
    
    private func setupRx() {
        let input = LoginViewModel.Input(loginTrigger: loginButton.rx.tap.asDriver(),
                                         signUpTrigger: signUpButton.rx.tap.asDriver(),
                                         email: emailTextField.rx.text.orEmpty.asDriver(),
                                         password: passwordTextField.rx.text.orEmpty.asDriver())
        let output = loginVM?.tranform(input: input)
        output?.error
            .drive(errorBinding)
            .disposed(by: disposeBag)
        output?.loading
            .drive(loadingBinding)
            .disposed(by: disposeBag)
        output?.formValid
            .drive(loginButton.rx.isEnabled)
            .disposed(by: disposeBag)
        output?.actionComplete
            .drive()
            .disposed(by: disposeBag)
    }
    
}
