//
//  LoginNavigator.swift
//  cleanarchitecture
//
//  Created by Dung Do on 30/12/2021.
//

import UIKit
import Domain

protocol LoginNavigator {
    func goToSignUp()
    func goToMain(user: User)
}

class DefaultLoginNavigator: LoginNavigator {
    
    private let navigation: UINavigationController
    private let useCaseProvider: UseCaseProvider
    
    init(useCaseProvider: UseCaseProvider, navigation: UINavigationController) {
        self.useCaseProvider = useCaseProvider
        self.navigation = navigation
    }
    
    func goToSignUp() {
        let signUpVC = SignUpViewController()
        let navigation = DefaultSignUpNavigator(useCaseProvider: useCaseProvider, navigation: navigation)
        signUpVC.signUpVM = SignUpViewModel(useCase: useCaseProvider.createLoginUseCase(), navigation: navigation)
        self.navigation.pushViewController(signUpVC, animated: true)
    }
    
    func goToMain(user: User) {
        let mainVC = MainViewController()
        let navigation = DefaultMainNavigator(useCaseProvider: useCaseProvider, navigation: navigation)
        mainVC.mainVM = MainViewModel(user: user, useCase: useCaseProvider.createLoginUseCase(), navigation: navigation)
        self.navigation.pushViewController(mainVC, animated: true)
    }
    
}
