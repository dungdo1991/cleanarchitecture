//
//  LoginViewModel.swift
//  cleanarchitecture
//
//  Created by Dung Do on 30/12/2021.
//

import UIKit
import Domain
import RxSwift
import RxCocoa

class LoginViewModel: ViewModelType {
    
    private let navigation: LoginNavigator
    private let useCase: LoginUseCase
    
    init(useCase: LoginUseCase, navigation: LoginNavigator) {
        self.useCase = useCase
        self.navigation = navigation
    }
    
    func tranform(input: Input) -> Output {
        let emailAndPassword = Driver.combineLatest(input.email,
                                                    input.password)
        let errorTracker = ErrorTracker()
        let activityIndicator = ActivityIndicator()
        
        let login = input.loginTrigger
            .withLatestFrom(emailAndPassword)
            .flatMap { email, password in
                self.useCase.login(email: email, password: password)
                    .track(activity: activityIndicator, error: errorTracker)
            }
            .do { user in
                self.navigation.goToMain(user: user)
            }
            .mapToVoid()
        
        let signUp = input.signUpTrigger.do(onNext: navigation.goToSignUp)
                
        let isValid = emailAndPassword.map { email, password in
            email.validateEmail() && password.validatePassword()
        }
        
        let actionComplete = Driver.merge(login, signUp)

        return Output(error: errorTracker.asDriver(),
                      loading: activityIndicator.asDriver(),
                      formValid: isValid,
                      actionComplete: actionComplete)
    }
    
}

extension LoginViewModel {
    struct Input {
        let loginTrigger: Driver<Void>
        let signUpTrigger: Driver<Void>
        let email: Driver<String>
        let password: Driver<String>
    }
    struct Output {
        let error: Driver<Error>
        let loading: Driver<Bool>
        let formValid: Driver<Bool>
        let actionComplete: Driver<Void>
    }
}
