//
//  MainViewModel.swift
//  cleanarchitecture
//
//  Created by Dung Do on 31/12/2021.
//

import UIKit
import Domain
import RxSwift
import RxCocoa

class MainViewModel: ViewModelType {
    
    private let navigation: MainNavigator
    private let useCase: LoginUseCase
    private let user: User
    
    init(user: User, useCase: LoginUseCase, navigation: MainNavigator) {
        self.user = user
        self.useCase = useCase
        self.navigation = navigation
    }
    
    func tranform(input: Input) -> Output {
        let errorTracker = ErrorTracker()
        let activityIndicator = ActivityIndicator()
        var users = [User]()
        
        let logout = input.logoutTrigger
            .flatMapLatest { _ in
                self.useCase.logout(email: self.user.email, password: self.user.password)
                    .track(activity: activityIndicator, error: errorTracker)
            }
            .do(onNext: navigation.goToLogin)
                
        let refresh = input.refreshTrigger
            .flatMapLatest { _ in
                self.useCase.users()
                    .track(activity: activityIndicator, error: errorTracker)
            }
            .map { newUsers -> [UserSection] in
                users.removeAll()
                users.append(contentsOf: newUsers)
                return self.convert(users: newUsers)
            }
        
        let nextPage = input.nextPageTrigger
            .flatMapLatest { _ in
                self.useCase.users()
                    .track(activity: activityIndicator, error: errorTracker)
            }
            .map { newUsers -> [UserSection] in
                users.append(contentsOf: newUsers)
                return self.convert(users: users)
            }
        
        let deleteUser = input.deleteUserTrigger
            .flatMapLatest { indexPath -> Driver<[UserSection]> in
                var sections = self.convert(users: users)
                let deletedUser = sections[indexPath.section].items.remove(at: indexPath.row)
                users.removeAll(where: { $0.id == deletedUser.id })
                
                return self.useCase.deleteUser(email: deletedUser.email)
                    .track(activity: activityIndicator, error: errorTracker)
                    .flatMapLatest { _ in
                        return Driver.just(sections)
                    }
            }
        
        let actionComplete = Driver.merge(logout)
        
        return Output(error: errorTracker.asDriver(),
                      loading: activityIndicator.asDriver(),
                      user: Driver.just(user),
                      sections: Driver.merge(refresh, nextPage, deleteUser),
                      refresh: activityIndicator.asDriver(),
                      actionComplete: actionComplete)
    }
    
    private func convert(users: [User]) -> [UserSection] {
        var sections: [UserSection] = []
        users.forEach { user in
            if let index = sections.firstIndex(where: { $0.header == "\(user.name.first!)" }) {
                sections[index].items.append(user)
            } else {
                sections.append(UserSection(header: "\(user.name.first!)", items: [user]))
            }
        }
        return sections.sorted(by: { $0.header < $1.header })
    }
    
}

extension MainViewModel {
    struct Input {
        let logoutTrigger: Driver<Void>
        let refreshTrigger: Driver<Void>
        let nextPageTrigger: Driver<Void>
        let deleteUserTrigger: Driver<IndexPath>
    }
    struct Output {
        let error: Driver<Error>
        let loading: Driver<Bool>
        let user: Driver<User>
        let sections: Driver<[UserSection]>
        let refresh: Driver<Bool>
        let actionComplete: Driver<Void>
    }
}
