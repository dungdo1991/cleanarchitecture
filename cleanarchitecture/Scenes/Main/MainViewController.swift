//
//  MainViewController.swift
//  cleanarchitecture
//
//  Created by Dung Do on 31/12/2021.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import PKHUD
import Domain

class MainViewController: BaseViewController {

    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet weak var helloLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var mainVM: MainViewModel?
    
    lazy var dataSource: RxTableViewSectionedAnimatedDataSource<UserSection> = {
        let dataSource = RxTableViewSectionedAnimatedDataSource<UserSection>(animationConfiguration: AnimationConfiguration(insertAnimation: .right, reloadAnimation: .automatic, deleteAnimation: .left)) { dataSource, tableView, indexPath, user in
            let cell = tableView.dequeueReusableCell(ofType: UserTableViewCell.self, at: indexPath)
            cell.user = user
            return cell
        } titleForHeaderInSection: { dataSource, index in
            return dataSource.sectionModels[index].header
        } canEditRowAtIndexPath: { _, indexPath in
            return true
        }
        return dataSource
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupRx()
        tableView.setContentOffset(CGPoint(x: 0, y :CGFloat.greatestFiniteMagnitude), animated: false)
    }
    
    private func setupView() {
        tableView.register(UserTableViewCell.self)
        tableView.delegate = self
        tableView.refreshControl = UIRefreshControl()
    }
    
    private func setupRx() {
        let refreshTriger = tableView.refreshControl!.rx.controlEvent(.valueChanged).asDriver()
        
        let input = MainViewModel.Input(logoutTrigger: logoutButton.rx.tap.asDriver(),
                                        refreshTrigger: refreshTriger,
                                        nextPageTrigger: tableView.rx.reachedBottom,
                                        deleteUserTrigger: tableView.rx.itemDeleted.asDriver())
        let output = mainVM?.tranform(input: input)
        output?.error
            .drive(errorBinding)
            .disposed(by: disposeBag)
        output?.loading
            .drive(loadingBinding)
            .disposed(by: disposeBag)
        output?.user
            .drive(userBinding)
            .disposed(by: disposeBag)
        output?.sections
            .drive(tableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
        output?.refresh
            .drive(tableView.refreshControl!.rx.isRefreshing)
            .disposed(by: disposeBag)
        output?.actionComplete
            .drive()
            .disposed(by: disposeBag)
    }
    
    var userBinding: Binder<User> {
        return Binder(self) { vc, user in
            self.helloLabel.text = "Hello \(user.name)!"
        }
    }
    
}

extension MainViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
}
