//
//  UserTableViewCell.swift
//  cleanarchitecture
//
//  Created by Dung Do on 07/01/2022.
//

import UIKit
import Domain

class UserTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    var user: User? {
        didSet {
            nameLabel.text = user?.name
            emailLabel.text = user?.email
        }
    }
    
}
