//
//  UserSection.swift
//  cleanarchitecture
//
//  Created by Dung Do on 07/01/2022.
//

import UIKit
import RxDataSources
import RealmSwift
import Domain

struct UserSection {
    var header: String
    var items: [Domain.User]
}

extension UserSection: AnimatableSectionModelType {
    typealias Item = Domain.User
    typealias Identity = String
    
    var identity: String {
        return header
    }
    
    init(original: UserSection, items: [Domain.User]) {
        self = original
        self.items = items
    }
}

extension Domain.User: IdentifiableType {
    public typealias Identity = UUID
    
    public var identity: UUID {
        return id
    }
    
    public static func ==(lhs: Domain.User, rhs: Domain.User) -> Bool {
      return lhs.id == rhs.id
    }
}
