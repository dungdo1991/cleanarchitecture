//
//  MainNavigator.swift
//  cleanarchitecture
//
//  Created by Dung Do on 31/12/2021.
//

import UIKit
import Domain

protocol MainNavigator {
    func goToLogin()
}

class DefaultMainNavigator: MainNavigator {
    
    private let navigation: UINavigationController
    private let useCaseProvider: UseCaseProvider
    
    init(useCaseProvider: UseCaseProvider, navigation: UINavigationController) {
        self.useCaseProvider = useCaseProvider
        self.navigation = navigation
    }
    
    func goToLogin() {
        navigation.popViewController(animated: true)
    }
    
}
