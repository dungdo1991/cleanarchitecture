//
//  SignUpViewController.swift
//  cleanarchitecture
//
//  Created by Dung Do on 30/12/2021.
//

import UIKit
import RxSwift
import RxCocoa
import PKHUD

class SignUpViewController: BaseViewController {
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var ageTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var genderSegment: UISegmentedControl!
    
    var signUpVM: SignUpViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupRx()
    }
    
    private func setupRx() {
        let input = SignUpViewModel.Input(name: nameTextField.rx.text.orEmpty.asDriver(),
                                          age: ageTextField.rx.text.orEmpty.asDriver(),
                                          email: emailTextField.rx.text.orEmpty.asDriver(),
                                          password: passwordTextField.rx.text.orEmpty.asDriver(),
                                          gender: genderSegment.rx.selectedSegmentIndex.asDriver(),
                                          signUpTrigger: signUpButton.rx.tap.asDriver())
        let output = signUpVM?.tranform(input: input)
        output?.error
            .drive(errorBinding)
            .disposed(by: disposeBag)
        output?.loading
            .drive(loadingBinding)
            .disposed(by: disposeBag)
        output?.formValid
            .drive(signUpButton.rx.isEnabled)
            .disposed(by: disposeBag)
        output?.actionComplete
            .drive()
            .disposed(by: disposeBag)
    }

}
