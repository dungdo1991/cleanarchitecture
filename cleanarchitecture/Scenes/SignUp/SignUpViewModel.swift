//
//  SignUpViewModel.swift
//  cleanarchitecture
//
//  Created by Dung Do on 30/12/2021.
//

import UIKit
import Domain
import RxSwift
import RxCocoa

class SignUpViewModel: ViewModelType {
    
    private let navigation: SignUpNavigator
    private let useCase: LoginUseCase
    
    init(useCase: LoginUseCase, navigation: SignUpNavigator) {
        self.useCase = useCase
        self.navigation = navigation
    }
    
    func tranform(input: Input) -> Output {
        let errorTracker = ErrorTracker()
        let activityIndicator = ActivityIndicator()
        let userInput = Driver.combineLatest(input.name,
                                             input.age,
                                             input.email,
                                             input.password,
                                             input.gender)
        
        let signUp = input.signUpTrigger
            .withLatestFrom(userInput)
            .map { (name, age, email, password, gender) in
                User(id: UUID(), name: name, age: Int(age) ?? 0, email: email, password: password, gender: gender == 0 ? .man : .unknown, address: Address(city: "Ho Chi Minh", country: "Viet Nam"))
            }
            .flatMapLatest { user in
                self.useCase.signUp(user: user)
                    .track(activity: activityIndicator, error: errorTracker)
            }
            .do(onNext: navigation.goToLogin)
                
        let isValid = userInput.map { name, age, email, password, gender in
            !name.isEmpty && (Int(age) ?? 0) > 0 && email.validateEmail() && password.validatePassword()
        }
        
        let actionComplete = Driver.merge(signUp)
                
        return Output(error: errorTracker.asDriver(),
                      loading: activityIndicator.asDriver(),
                      formValid: isValid,
                      actionComplete: actionComplete)
    }
    
}

extension SignUpViewModel {
    struct Input {
        let name: Driver<String>
        let age: Driver<String>
        let email: Driver<String>
        let password: Driver<String>
        let gender: Driver<Int>
        let signUpTrigger: Driver<Void>
    }
    struct Output {
        let error: Driver<Error>
        let loading: Driver<Bool>
        let formValid: Driver<Bool>
        let actionComplete: Driver<Void>
    }
}
