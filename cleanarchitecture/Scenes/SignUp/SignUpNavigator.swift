//
//  SignUpNavigator.swift
//  cleanarchitecture
//
//  Created by Dung Do on 30/12/2021.
//

import UIKit
import Domain

protocol SignUpNavigator {
    func goToLogin()
}

class DefaultSignUpNavigator: SignUpNavigator {
    
    private let navigation: UINavigationController
    private let useCaseProvider: UseCaseProvider
    
    init(useCaseProvider: UseCaseProvider, navigation: UINavigationController) {
        self.useCaseProvider = useCaseProvider
        self.navigation = navigation
    }
    
    func goToLogin() {
        navigation.popViewController(animated: true)
    }
    
}
